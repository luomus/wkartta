# WKartta

WKartta on web-sivuihin upotettava karttatyökalu, jonka avulla käyttäjä voi tehdä kartalle merkintöjä niiden samalla
välittyessä web-sovellukseen, johon karttatyökalu on upotettu.

## CHANGELOG

### 24.11.2021

laji-map laitettiin tulemaan cdn:stä, jotta tätä pakettia ei tarvitse päivittää erikseen kun laji-map tarvitsee päivittää.

### 21.9.2018

WKartta vaihdettiin käyttämään Luomuksen ylläpitämää laji-map-kirjastoa. Alla listattu API-muutoksia:
* `mapOptions` välittyy ennen Leaflettia LajiMapille. LajiMap välittää optionsit joita ei tunnista Leafletille.
* Leaflet kartan (*.map) lisäksi LajiMap-instanssiin pääsee käsiksi *.lajimap 

## Asennus ja kehitysympäristö
**Jos olet upottamassa WKarttaa sivuillesi, siirry suoraan osioon asiakaskirjaston käyttäminen**

WKartan käyttäminen vaatii sen lähdekoodin paketointia Webpack-työkalulla. Mukana tulevalla Grunt-konfiguraatiolla se
onnistuu suhteellisen pienellä vaivalla. Koneella tulee olla node asennettuna ja vaikka nodea pystyy pyörittämään windowsissa on se huomattavasti nopeampi linux ympäristössä.

Lisää Google API avaimesi tiedostoon `properties.json`. Katso esimerkkitiedosto `properties.json.example`.

```
$ git clone git@bitbucket.org:luomus/wkartta.git
$ cd wkartta
$ npm install --dev # Toimii ainakin node versiolla 8.
$ npm start
```

Tämän jälkeen WKartan pitäisi olla käynnissä osoitteessa http://localhost:8000

### Asennus palvelimelle

```
$ grunt dist
$ cp -R dist/* /var/www/html/wkartta/
```

## Asiakaskirjaston käyttäminen

WKartan upottaminen vaatii webpackin paketoiman consumer.js-kirjaston lataamista upottavalla sivulla. 
Kirjasto ei sisällä ulkoisia riippuvuuksia. Kirjasto lataa WKartan haluttuun iframe-elementtiin käyttäen annettua konfiguraatiota.

Valmiiksi käännetyn consumer.js-tiedoston voi linkata tai hakea [tästä osoitteesta](https://koivu.luomus.fi/wkartta/js/consumer.js)
tai vaihtoehtoisesti generoida itse aiemmin kuvaillulla tavalla.

### Asiakaskirjaston rajapinta

*WK.Map(<HTMLElement|String> mapElem, <Options> options?)*

**Parametrit*:*
*mapElem* - IFrame-elementin id tai vaihtoehtoisesti IFramen HTMLElement
*options* - Kartan konfiguraatio-optiot

**Esimerkki**

Luodaan kartta johon voi piirtää vain yhden pisteen ("markkerin") jolle ei voi antaa tekstimerkintöjä. 
Karttapohjana käytetään maanmittauslaitoksen karttapohjaa ja oletuksena kartta osoittaa luonnontieteelliseen keskusmuseoon

```
<html>
<head>
...
<script type="text/javascript" src="scripts/consumer.js"></script>
...
</head>
...
<iframe id="map" src="https://www.luomus.fi/wkartta/map.html"></iframe>
<script type="text/javascript">
    var mapElement = document.getElementById('map');
    var map = new (new WK()).Map(mapElement, {
        single: true,
        annotate: false,
        mapTypes: ['maanmittaus'],
        mapOptions: {
            center: [60.171249, 24.931176]
        }
    });
    map.onChange(function(entries) {
        console.log('karttaa päivitettiin!');
        handleEntries(entries);
    });
</script>
...
</html>
```


**Options**

```
Nimi        Tyyppi                  Oletus              Kuvaus
===================================================================================
simple      boolean                 false               Pakotetaanko yksinkertainen tila (pelkät markkerit)
single      boolean                 false               Voiko kartalle voi piirtää vain yhden kuvion tai pisteen
annotate    boolean                 true                Tuleeko merkinnöille olla mahdollista kirjoittaa tekstimerkintöjä
edit        boolean                 false               Voiko karttalle piirrettyjä kuvioita muokata
mapOptions      MapOptions          kts. MapOptions     LajiMapille ja sen jälkeen Leafletin kartalle välitettävät asetukset
locationTypes   LocationTypes       kts. LocationTypes  Määrittelee mitä kuvioita käyttäjä voi piirtää kartalle
mapTypes        [String] | 'auto'   ['osm']             Mitä karttapohjia voidaan käyttää ('mm', 'osm'), ensimmäinen oletuksena
```

**Käytettävissä olevat karttapohjat**

```
Avain       Url                                                                 Kuvaus
===================================================================================================================
'mm'        http://tiles.kartat.kapsi.fi/peruskartta/{z}/{x}/{y}.jpg            Maanmittauslaitoksen peruskartta
'osm'       http://a.tile.openstreetmap.org/{z}/{x}/{y}.png                     OpenStreetMap-projektin peruskartta
```

Antamalla mapTypes-parametrin 'auto' käytetään käyttäjän näkymän perusteella joko
OpenStreetMap-karttapohjaa tai maanmittauslaitoksen peruskarttaa. Jos käyttäjän näkymä
on Suomen sisällä ja zoomaustaso on tarpeeksi lähellä käytetään peruskarttaa kun taas
ulos zoomattuna tai ulkomailla käytetään OpenStreetMap-karttapohjaa.


**MapOptions**

```
Nimi        Tyyppi             Oletus          Kuvaus
================================================================
center      [Number, Number]   "suomessa"      Kartan alkusijainti (itä-, pohjoisdesimaalikoordinaatti (WGS-84))
zoom        Number             8?              Kartan alkuzoomi
minZoom     Number             null            Minimizoomi 
maxZoom     Number             20              Maksimizoomi
maxBounds   [[Number, Number], null            Kartan vierityksen rajat (lounaskulma, koilliskulma)
             [Number, Number]] 
```

**LocationTypes**

```
Nimi        Tyyppi          Oletus          Kuvaus
==================================================
lineString  boolean         false           Voiko kartalle piirtää polkuja
point       boolean         true            Voiko kartalle piirtää pisteitä
```

**Metodit**

*.onChange(<function(<[MapEntry]> mapEntries)> callback)*
Rekisteröi takaisinkutsufunktion jota kutsutaan aina kun käyttäjä muuttaa kartan kuvioita tai kuvioihin liittyiä
tekstimerkintöjä, takaisinkutsufunktiolle välitetään Objekti joka sisältää kartalle merkityt kuviot ja tekstimerkinnät

*.update(<[MapEntry]> mapEntries)*
Päivitä kartan tila vastaamaan parametrinä annettua merkintäkokoelmaa

*.geocode(String query, Number zoomLevel?)*
Paikallista vapaamuotoisen query-parametrin avulla jokin sijainti ("Helsinki", "Eduskuntatalo" jne.)
ja siirrä karttanäkymä sijainnin kohdalle. Vapaaehtoinen zoomLevel-parametri määrittelee
käytettävän zoomin.

*.setView(LatLng center?, Number zoomLevel?)*
Aseta karttanäkymä parametrien mukaisesti. Kumpikin parametri on vapaaehtoinen.


**MapEntry**

```
Nimi            Tyyppi          Kuvaus
==================================================
locationType    String          WKT:n mukainen koordinaattien tyyppi (esim. POINT, LINESTRING, POLYGON)
location        WKT             Koordinaattien tyyppiä vastaava sijaintieto numeroina listoissa.
                                Esimerkiksi POINTilla [30, 10] tai LINESTRINGillä 
                                [[30, 10], [10,30], [40, 40]] (kolmen pisteen polku)
notes           String          Käyttäjän tekemä karttamerkinnän vapaamuotoinen  tekstimerkintä
```
