(function () {
    var noop = function () {
    };
    var DEFAULT_CLIENT_CONFIG = {
        single: true,
        annotate: true,
        edit: false,
        mapOptions: {
            center: [60, 20],
            zoom: 8,
            minZoom: null,
            maxZoom: null,
            maxBounds: null
        },
        locationTypes: {
            lineString: false,
            point: true
        },
        mapTypes: ['osm']
    };

    var DEFAULT_VIEW_CONFIG = {
        mapOptions: {},
        mapTypes: []
    };

    var extendDefaultConfig = function (newOptions) {
        // Clone the default config so it will not get modified
        return $.extend($.extend({}, DEFAULT_CLIENT_CONFIG), newOptions);
    };

    var MockView = function () {
        this.layers = [];
        this.clear = function () {
            this.layers = [];
        };
        this.addLayer = function (layer) {
            this.layers.push(layer);
            // Simulate leaflet id generation
            if (layer._leaflet_id === undefined) {
                layer._leaflet_id = Math.floor(Math.random() * 1000);
            }
        };
        this.reCenter = noop;
    };
    var mockMap = {
        addLayer: noop,
        addControl: noop,
        on: noop
    };

    var getInnerLayers = function (layerGroup) {
        var layers = [];
        layerGroup.eachLayer(function (layer) {
            layers.push(layer);
        });

        return layers;
    };

    var countLayers = function (layers) {
        return getInnerLayers(layers).length;
    };

    TestCase("Controller tests", {
        getMarker: function (latLng) {
            var marker = new L.Marker(latLng);
            marker.layerType = 'marker';
            marker._leaflet_id = Math.floor(Math.random() * 1000);
            return marker;
        },
        "test presence": function () {
            assertNotUndefined(WKProvider);
            assertNotUndefined(WKProvider.app);
            assertNotUndefined(WKProvider.App);
        },

        "test single": function () {
            var app = new WKProvider.App();
            var controller = new app.Controller(MockView, extendDefaultConfig({single: true}));
            var mockView = controller._view;
            assertEquals(0, mockView.layers.length);
            assertEquals(0, controller.exportData().length);

            $(mockView).trigger('created', this.getMarker([60.0, 20.0]), 'marker');
            assertEquals(1, mockView.layers.length);
            assertEquals(1, controller.exportData().length);

            $(mockView).trigger('created', this.getMarker([61.0, 21.0]), 'marker');
            assertEquals(1, mockView.layers.length);
            assertEquals(1, controller.exportData().length);
        },
        "test multi": function () {
            var app = new WKProvider.App();
            var controller = new app.Controller(MockView, extendDefaultConfig({single: false}));
            var mockView = controller._view;
            assertEquals(0, mockView.layers.length);
            assertEquals(0, controller.exportData().length);

            $(mockView).trigger('created', this.getMarker([60.0, 20.0]), 'marker');
            assertEquals(0, mockView.layers.length);
            assertEquals(1, controller.exportData().length);

            $(mockView).trigger('created', this.getMarker([61.0, 21.0]), 'marker');
            assertEquals(0, mockView.layers.length);
            assertEquals(2, controller.exportData().length);
        },
        "test update": function () {
            var app = new WKProvider.App();
            var controller = new app.Controller(MockView, DEFAULT_CLIENT_CONFIG);
            var mockView = controller._view;

            var changes = 0;
            var incChanges = function () {
                changes++;
            };
            $(controller).on('changed', incChanges);
            assertEquals(0, changes);

            var marker = this.getMarker([60.0, 20.0]);
            $(mockView).trigger('created', marker);
            assertEquals(1, controller.exportData().length);
            assertEquals(1, changes);

            marker.setLatLng([61.0, 21.0]);
            $(mockView).trigger('edited', marker);
            assertEquals(1, controller.exportData().length);
            assertEquals(2, changes);

            $(mockView).trigger('notesUpdated', marker, 'a note');
            assertEquals(1, controller.exportData().length);
            assertEquals(3, changes);

            $(mockView).trigger('deleted', marker);
            assertEquals(0, controller.exportData().length);
            assertEquals(4, changes);
        },

        "test updateNotes": function () {
            var app = new WKProvider.App();
            var controller = new app.Controller(MockView, DEFAULT_CLIENT_CONFIG);
            var mockView = controller._view;

            var marker = this.getMarker([60.0, 20.0]);
            $(mockView).trigger('created', marker);
            assertEquals('', controller.exportData()[0].notes);

            $(mockView).trigger('notesUpdated', [marker, 'a note']);
            assertEquals('a note', controller.exportData()[0].notes);
        }
    });



    TestCase('Map option tests', {
        OptionSensingMockMap: function (div, options) {
            var sensingMockMap = $.extend({}, mockMap);
            sensingMockMap.controls = [];
            sensingMockMap.options = options;
            sensingMockMap.addControl = function (control) {
                this.controls.push(control);
            }
            return sensingMockMap;
        },
        "test mapOptions": function () {
            var options = {
                mapOptions: {
                    center: [66.0, 22.0],
                    zoom: 10,
                    minZoom: 4,
                    maxZoom: 6,
                    maxBounds: [
                        [60.0, 20.0],
                        [70, 30]
                    ] // valid bounds
                },
                locationTypes: {
                    lineString: true,
                    point: false
                },
                mapTypes: []
            };

            var provider = new WKProvider.App(null, this.OptionSensingMockMap);
            var controller = new provider.Controller(provider.View, options);
            var view = controller._view;
            assertEquals(1, view._map.controls.length);
            var drawOptions = view._map.controls[0].options.draw;
            assertTrue(drawOptions.polyline !== false);
            assertEquals(options.locationTypes.point, drawOptions.marker);
            assertTrue(view._map.options.maxBounds.isValid());
            assertEquals(new L.LatLng(66.0, 22.0), view._map.options.center);

        },

        "test mapLayers": function () {

        }
    });

    TestCase('mapTypes tests', {
        getMapForTypes: function (types) {
            var options = extendDefaultConfig({});
            options.mapTypes = types;
            var provider = new WKProvider.App(null, function () {
                this.layers = [];
                this.controls = [];
                this.on = noop;
                this.addControl = function (control) {
                    this.controls.push(control);
                };
                this.addLayer = function (layer) {
                    this.layers.push(layer);
                };
            });

            var controller = new provider.Controller(provider.View, options);

            return controller._view._map;
        },

        "test singleType": function () {
            var gotMockMap = this.getMapForTypes(['mm']);
            assertEquals(1, gotMockMap.controls.length);
            assertEquals(3, gotMockMap.layers.length);
        },
        "test twoTypes": function () {
            var gotMockMap = this.getMapForTypes(['mm', 'osm']);
            assertEquals(2, gotMockMap.controls.length);
            assertEquals(3, gotMockMap.layers.length);
        },
        "test noTypes": function () {
            var gotMockMap = this.getMapForTypes([]);
            assertEquals(1, gotMockMap.controls.length);
            assertEquals(2, gotMockMap.layers.length);
        },
        "test invalidType": function () {
            try {
                this.countForTypes(['osm', 'invalidtype']);
                fail('did not raise exception for invalid map layer type');
            } catch (err) {
                // Do nothing
            }

        }
    });

    TestCase('UI tests', {
        "test loading": function () {
            var loadingDiv = $('<div id="loading"></div>');
            var provider = new WKProvider.App();
            $(document.body).append(loadingDiv);
            var initialized = false;
            provider.init = function () {
                initialized = true;
            };

            provider._getUrl = function () {
                return new Uri("http://example.com/");
            };
            provider.main();
            assertTrue(loadingDiv.is(':visible'));
            assertFalse(initialized);

            provider._getUrl = function () {
                return new Uri("http://example.com/?config=%7B%22foo%22%3Atrue%2C%22bar%22%3A%22baz%22%7D");
            };
            provider.main();
            assertFalse(loadingDiv.is(':visible'));
            assertTrue(initialized);
        },

        MockMapWithListener: function () {
            var mockMapWithListener = $.extend({}, mockMap);
            mockMapWithListener.listeners = {};
            mockMapWithListener.layers = [];
            mockMapWithListener.addLayer = function (layer) {
                this.layers.push(layer);
            };
            mockMapWithListener.on = function (event, fn) {
                if (this.listeners[event] === undefined) {
                    this.listeners[event] = [fn];
                } else {
                    this.listeners[event].push(fn);
                }

            };
            mockMapWithListener.removeLayer = function (layer) {
                delete layers[layers.index(layer)];
            };

            return mockMapWithListener;
        },

        "test notes": function () {
            $(document.body).append($('<textarea id="note"></textarea>'));
            var popupListener;
            var provider = new WKProvider.App(this.MockMapWithListener, this.MockMapWithListener);
            var view = new provider.View(DEFAULT_VIEW_CONFIG, function () {
                return 'foo';
            });
            var listeners = view._map.listeners;

            var trigger = function (event, data) {
                $.each(listeners[event], function () {
                    this(data);
                });
            };

            var receivedNote;
            $(view).on('notesUpdated', function (e, layer, note) {
                receivedNote = note;
            });

            trigger('popupopen', {popup: {_source: {}, setContent: function () {
            }}});
            assertEquals('foo', $('#note').val());
            // Simulate user changing the note
            $('#note').val('bar');
            $('#note').change();
            assertEquals('bar', receivedNote);
        },
        "test pathNumbersManual": function () {
            var provider = new WKProvider.App(null, this.MockMapWithListener);

            var view = new provider.View(DEFAULT_VIEW_CONFIG, null);
            var listeners = view._map.listeners;
            var layers = view._map.layers;
            var trigger = function (event, data) {
                $.each(listeners[event], function () {
                    this(data);
                });
            };

            assertEquals(2, layers.length);
            var numbersLayer = layers[1];
            assertEquals(0, countLayers(numbersLayer));
            view.addLayer(new L.Polyline([
                [60.0, 20.0],
                [61.0, 21.0]
            ]), 'polyline');
            assertEquals(1, countLayers(numbersLayer));
            assertEquals(2, countLayers(getInnerLayers(numbersLayer)[0]));

            view.clear();
            assertEquals(2, layers.length);
            assertEquals(0, countLayers(numbersLayer));


        },
        "test pathNumbers": function () {
            var provider = new WKProvider.App(null, this.MockMapWithListener);

            var view = new provider.View(DEFAULT_VIEW_CONFIG, null);
            var listeners = view._map.listeners;
            var layers = view._map.layers;
            var trigger = function (event, data) {
                $.each(listeners[event], function () {
                    this(data);
                });
            };

            assertEquals(2, layers.length);
            var numbersLayer = layers[1];
            assertEquals(0, countLayers(numbersLayer));
            var polyline = new L.Polyline([
                [60.0, 20.0],
                [61.0, 21.0]
            ]);
            trigger('draw:created', {layerType: 'polyline', layer: polyline});
            assertEquals(1, countLayers(numbersLayer));
            assertEquals(2, countLayers(getInnerLayers(numbersLayer)[0]));

            polyline.setLatLngs([
                [60.0, 20.0],
                [60.5, 20.5],
                [61.0, 21.0]
            ]);
            trigger('draw:edited', {layers: new L.LayerGroup([polyline])});
            assertEquals(1, countLayers(numbersLayer));
            assertEquals(3, countLayers(getInnerLayers(numbersLayer)[0]));

            polyline.setLatLngs([
                [60.5, 20.5],
                [61.0, 21.0]
            ]);
            trigger('draw:edited', {layers: new L.LayerGroup([polyline])});
            assertEquals(1, countLayers(numbersLayer));
            assertEquals(2, countLayers(getInnerLayers(numbersLayer)[0]));

            trigger('draw:deleted', {layers: new L.LayerGroup([polyline])});
            assertEquals(2, layers.length);
            assertEquals(0, countLayers(numbersLayer));
        }
    });

    TestCase('Integration tests', {
        "test geolocate": function () {
            $(document.body).append('<div id="map"></div>');
            var gotQuery;
            var geocode = function (query, callback) {
                gotQuery = query;
                var results = [
                    {
                        formatted_address: 'SUAMI',
                        geometry: {
                            location: {
                                lat: function () {
                                    return 60.0;
                                },
                                lng: function () {
                                    return 20.0;
                                }
                            }
                        }
                    }
                ];

                callback(results, 'OK');
            };
            var MockGeocoder = {
                geocode: geocode
            };

            var mockEnder = {
                receiveMessage: function (callback) {
                    this.callback = callback;
                },
                postMessage: function (data, parentUrl) {
                    this.data = data;
                }
            };
            var provider = new WKProvider.App(mockEnder, L.Map, MockGeocoder);


            provider.init(DEFAULT_CLIENT_CONFIG);
            mockEnder.callback({data: JSON.stringify({geocode: {query: 'aaa', zoomLevel: 5}})});
            assertEquals({address: 'aaa'}, gotQuery);
            mockEnder.callback({data: JSON.stringify({setView:{center:[20.0, 60.0], zoomLevel: 3}})})
        },
        // End-to-end test to ensure that observation entries are imported
        "test receiveData": function () {
            var hookMap;
            $(document.body).append('<div id="map"></div>');
            var HookMap = function (div, options) {
                hookMap = new L.Map(div, options);
                return hookMap;
            };

            var mockEnder = {
                receiveMessage: function (callback) {
                    this.callback = callback;
                },
                postMessage: function (data, parentUrl) {
                    this.data = data;
                }
            };

            var provider = new WKProvider.App(mockEnder, HookMap);
            var controller;
            var OldController = provider.Controller;
            provider.Controller = function (View, options) {
                controller = new OldController(View, options);
                return controller;
            };
            provider.prototype = provider.Controller;
            provider.init(DEFAULT_CLIENT_CONFIG);
            assertNotUndefined(controller);
            assertNotUndefined(hookMap);
            var marker = new L.Marker([60.0, 20.0]);
            marker._leaflet_id = 1;
            hookMap.fire('draw:created', {layer: marker, layerType: 'marker'});

            var expectedData = [
                {
                    location: [60.0, 20.0],
                    locationType: 'POINT',
                    notes: ''
                }
            ];
            assertEquals(expectedData, JSON.parse(mockEnder.data));

            var editedGroup = new L.FeatureGroup();
            marker.setLatLng([61.0, 21.0]).addTo(editedGroup);

            hookMap.fire('draw:edited', {layers: editedGroup});
            expectedData[0].location = [61.0, 21.0];
            assertEquals(expectedData, JSON.parse(mockEnder.data));

            $(document.body).append($('<textarea id="note"></textarea>'));
            hookMap.fire('popupopen', {popup: {_source: marker, setContent: noop}});
            $('#note').val('new note').change();
            expectedData[0].notes = 'new note';
            assertEquals(expectedData, JSON.parse(mockEnder.data));

            hookMap.fire('draw:deleted', {layers: editedGroup});
            expectedData = [];
            assertEquals(expectedData, JSON.parse(mockEnder.data));
        }
    });
})();
