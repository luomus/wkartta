'use strict';

var assert = require('chai').assert;
var $ = require('jquery');
var L = require('leaflet');
var Controller = require('../src/provider/controller');
var _ = require('lodash');

describe('Controller', function () {
    var mockView;
    var controller;
    var injectedController = function (callback) {
        var mockGoogle = {
            maps: {
                Geocoder: function () {
                    this.geocode = callback ? callback : function() {
                    };
                },
                GeocoderStatus: {
                    OK: 'OK',
                    ZERO_RESULTS: 'ZERO_RESULTS'
                }
            }
        };
        var ControllerInjector = require('inject?google!../src/provider/controller');
        return ControllerInjector({
            'google': mockGoogle
        });
    };
    beforeEach(function () {
        mockView = {
            layers: [],
            notifications: [],
            clear: function () {
                this.layers = [];
            },
            addLayer: function (layer) {
                this.layers.push(layer);
                layer._leaflet_id = this.layers.length;
            },
            reCenter: function () {
                this.center = Array.prototype.slice.call(arguments);
            },
            notify: function (notification) {
                this.notifications.push(notification);
            }
        };

        var Controller = injectedController();
        controller = new Controller(mockView, {single: false});
    });

    var makeMarker = function (latLng) {
        var marker = new L.Marker(latLng);
        marker.layerType = 'marker';
        marker._leaflet_id = Math.floor(Math.random() * 1000);
        return marker;
    };

    describe('marker creation', function () {
        it("should only allow a single marker", function () {
            var Controller = injectedController();
            controller = new Controller(mockView, {single: true});
            assert.equal(0, mockView.layers.length);
            assert.equal(0, controller.exportData().length);

            $(mockView).trigger('created', makeMarker([60.0, 20.0]), 'marker');
            assert.equal(1, mockView.layers.length);
            assert.equal(1, controller.exportData().length);

            $(mockView).trigger('created', makeMarker([61.0, 21.0]), 'marker');
            assert.equal(1, mockView.layers.length);
            assert.equal(1, controller.exportData().length);
            assert.deepEqual([61.0, 21.0], controller.exportData()[0].location);
        });
        it("should allow multiple multiple markers", function () {
            assert.equal(0, mockView.layers.length);
            assert.equal(0, controller.exportData().length);

            var locations = [[60.0, 20.0], [61.0, 21.0]];
            $(mockView).trigger('created', makeMarker(locations[0]), 'marker');
            assert.equal(0, mockView.layers.length);
            assert.deepEqual(locations.slice(0, 1), _.map(controller.exportData(), 'location'));

            $(mockView).trigger('created', makeMarker(locations[1]), 'marker');
            assert.equal(0, mockView.layers.length);
            assert.deepEqual(locations.sort(), _.map(controller.exportData(), 'location').sort());
        });
    });

    describe('export', function () {
        it('should export markers', function () {
            var mockView = {};
            var Controller = injectedController();
            var controller = new Controller(mockView, {single: false});

            var coords = [60.0, 20.0];
            var marker = new L.Marker(coords);
            marker._leaflet_id = 1;
            marker.layerType = 'marker';
            $(mockView).trigger('created', marker);

            var entries = controller.exportData();
            assert.equal(1, entries.length);
            var expectedEntry = {
                location: coords,
                notes: '',
                locationType: 'POINT'
            };
            assert.deepEqual(expectedEntry, entries[0]);
        });
        it('should export polylines', function () {
            var mockView = {};
            var Controller = injectedController();
            var controller = new Controller(mockView, {single: false});
            var coords = [
                [60.0, 20.0],
                [61.0, 21.0],
                [62.0, 22.0]
            ];

            var polyline = new L.Polyline(coords);
            polyline._leaflet_id = 1;
            polyline.layerType = 'polyline';
            $(mockView).trigger('created', polyline);

            var entries = controller.exportData();
            assert.equal(1, entries.length);
            var expectedEntry = {
                location: coords,
                notes: '',
                locationType: 'LINESTRING'
            };
            assert.deepEqual(expectedEntry, entries[0]);
        });
    });

    describe('import', function () {
        it("should import markers", function () {
            controller.importData([
                {
                    notes: 'a note',
                    locationType: 'POINT',
                    location: [60.0, 20.0]
                }
            ]);
            assert.equal(1, mockView.layers.length);
            assert.deepEqual(new L.LatLng(60.0, 20.0), mockView.layers[0].getLatLng());
            assert.equal(1, controller.exportData().length);
            assert.equal('a note', controller.exportData()[0].notes);

        });
        it("should import linestrings", function () {
            controller.importData([
                {
                    notes: 'an another note',
                    locationType: 'LINESTRING',
                    location: [
                        [60.0, 20.0],
                        [61.0, 21.0]
                    ]
                }
            ]);
            assert.equal(1, mockView.layers.length);
            assert.deepEqual([new L.LatLng(60.0, 20.0), new L.LatLng(61.0, 21.0)],
                mockView.layers[0].getLatLngs());
            assert.equal(1, controller.exportData().length);
            assert.equal('an another note', controller.exportData()[0].notes);
        });
        it("should import mixed data", function () {
            controller.importData([
                {
                    notes: 'an another note',
                    locationType: 'LINESTRING',
                    location: [
                        [60.0, 20.0],
                        [61.0, 21.0]
                    ]
                },
                {
                    notes: 'a note',
                    locationType: 'POINT',
                    location: [60.0, 20.0]
                }
            ]);
            assert.equal(2, mockView.layers.length);
            assert.deepEqual([new L.LatLng(60.0, 20.0), new L.LatLng(61.0, 21.0)],
                mockView.layers[0].getLatLngs());
            assert.deepEqual(new L.LatLng(60.0, 20.0), mockView.layers[1].getLatLng());
            assert.equal(2, controller.exportData().length);

            // Exported data order not deterministic
            assert.deepEqual(['a note', 'an another note'], _.map(controller.exportData(), 'notes').sort());
        });
    });

    describe('geocoder', function () {
        var createMock = function (geocodeFn) {
            var Controller = injectedController(geocodeFn);
            return new Controller(mockView);
        };
        it('should throw on an error', function () {
            assert.throws(function () {
                var controller = createMock(function (query, callback) {
                    callback(['foo', 'blah'], 'CRAZY ERROR');
                });
                controller.geocode('yay');
            });
        });
        it('should center to the first result and notify', function () {
            var address = 'AN ADDRESS!!';
            var controller = createMock(function (query, callback) {
                callback([{
                    formatted_address: address,
                    geometry: {
                        location: {
                            lat: function () {
                                return 60.0;
                            },
                            lng: function () {
                                return 20.0;
                            }
                        }
                    }
                }], 'OK');
            });

            controller.geocode('foo', 5);
            assert.deepEqual([[60.0, 20.0], 5], mockView.center);
            assert.deepEqual([address], mockView.notifications);
        });
        it('should work with no results', function () {
            var controller = createMock(function (query, callback) {
                callback([], 'ZERO_RESULTS');
            });

            controller.geocode('foo', 5);
            assert.isUndefined(mockView.center);
            assert.isTrue(mockView.notifications.length === 1);
        });
    });
});