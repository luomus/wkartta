'use strict';

var EXAMPLE_URL = 'http://example.com';
var assert = require('chai').assert;
var WK = require('../src/consumer');
var JSON3 = require('json3');
var jsUri = require('jsuri');
var _ = require('lodash');

describe("consumer", function () {
    it("should be defined", function () {
        assert.isFunction(WK);
    });
    describe('in initialization', function () {
        var elem;
        beforeEach(function () {
            elem = document.createElement('iframe');
            elem.src = EXAMPLE_URL;
        });

        it("should bind with an elem", function () {
            (new WK()).Map(elem);
            assert.isTrue(elem.src.indexOf(EXAMPLE_URL) === 0 && elem.src.length > EXAMPLE_URL.length);
        });
        it("should bind with an id", function () {
            var elemWithId = document.createElement('iframe');
            elemWithId.src = EXAMPLE_URL;
            elemWithId.id = 'map';
            document.body.appendChild(elemWithId);
            var map = new (new WK()).Map('map');
            assert.isTrue(elemWithId.src.indexOf(EXAMPLE_URL) === 0 && elemWithId.src.length > EXAMPLE_URL.length);
        });
        it("should raise an exception with an invalid element", function () {
            assert.throws(function() {
                (new WK()).Map('doesnotexist');
            });
        });
        it("delivers parent url", function () {
            (new WK()).Map(elem);
            var parentUrl = decodeURIComponent(new jsUri(elem.src).anchor());
            assert.equal(document.location.href, parentUrl);
        });
        it("delivers configuration", function () {
            var options = _.extend((new WK()).DEFAULT_OPTIONS, {
                single: true,
                edit: true,
                // extra hard parsing challenge
                mapTypes: ['@ty"pe;}{\''],
                mapOptions: {
                    center: [60.0, 20.0]
                },
                layerTypes: {
                    lineString: true,
                    marker: false
                }
            });
            (new WK()).Map(elem, options);
            var iFrameSrc = new jsUri(elem.src);
            assert.equal(1, iFrameSrc.getQueryParamValues('config').length);
            var deliveredOptions = JSON3.parse(decodeURIComponent(iFrameSrc.getQueryParamValue('config')));
            assert.deepEqual(options, deliveredOptions);
        });
    });

    describe('after initialization', function () {
        var gotMsgs;
        var msgCallback;
        var map;
        beforeEach(function () {
            var elem = document.createElement('iframe');
            elem.src = EXAMPLE_URL;
            gotMsgs = [];

            var enderMock = {
                postMessage: function (msg) {
                    gotMsgs.push(msg);
                },
                receiveMessage: function (gotCallback) {
                    msgCallback = gotCallback;
                }
            };
            var WKInjector = require('inject?postmessage!../src/consumer');
            var WK = WKInjector({
                'postmessage': enderMock
            });

            map = new (new WK()).Map(elem);
            assert.isDefined(msgCallback);
            msgCallback({data: JSON3.stringify({ready: true})});
        });

        it("should trigger a postmessage on update", function () {
            map.update([]);
            assert.isTrue(gotMsgs.length === 1, "did not receive a message");
            assert.deepEqual({data: []}, JSON3.parse(gotMsgs[0]));
        });
        it("emits updates to registered onChange listeners", function () {
            var gotEntries;
            map.onChange(function (entries) {
                gotEntries = entries;
            });

            var expectedEntries = [
                {
                    "locationType": "POINT",
                    "location": [
                        60.133299237518216,
                        22.9833984375
                    ],
                    "notes": ""
                }
            ];
            msgCallback({data: JSON3.stringify({data: expectedEntries})});
            assert.deepEqual(gotEntries, expectedEntries);
        });
        it("should send geocode commands", function () {
            map.geocode('kerava', 5);
            assert.equal(1, gotMsgs.length);
            assert.deepEqual({geocode: {query: 'kerava', zoomLevel: 5}}, JSON3.parse(gotMsgs[0]));
        });
        it("should send setview commands", function () {
            map.setView([60.0, 20.0], 6);
            assert.equal(1, gotMsgs.length);
            assert.deepEqual({setView: {center: [60.0, 20.0], zoomLevel: 6}}, JSON3.parse(gotMsgs[0]));
        });
    });
});
