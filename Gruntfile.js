'use strict';

var webpack = require('webpack');
var _ = require('lodash');

module.exports = function (grunt) {
    grunt.loadNpmTasks("grunt-webpack");
    grunt.loadNpmTasks('grunt-contrib-copy');
    grunt.loadNpmTasks('grunt-contrib-clean');
    grunt.loadNpmTasks('grunt-contrib-connect');
    grunt.loadNpmTasks('grunt-contrib-watch');

    var devOptions = {
        devtool: 'eval',
        output: {
            path: __dirname + '/target/js/'
        }
    };
    var distOptions = {
        devtool: 'source-map',
        output: {
            path: __dirname + '/dist/js/'
        }
    };

    var consumerOptions = {
        entry: './src/consumer.js',
        output: {
            filename: "consumer.js",
            // export itself to a global var
            libraryTarget: "var",
            // name of the global var
            library: "WK"
        }
    };
    var providerOptions = {
        entry: './src/provider/provider.js',
        output: {
            filename: "provider.js",
            // export itself to a global var
            libraryTarget: "var",
            // name of the global var
            library: "WKMap"
        },
        externals: {
            'leaflet': 'L'
        }
    };
    var viewerOptions = {
        entry: './src/viewer.js',
        output: {
            filename: "viewer.js",
            // export itself to a global var
            libraryTarget: "var",
            // name of the global var
            library: "WKViewer"
        }
    };

    grunt.initConfig({
        connect: {
            server: {
                options: {
                    base: 'target'
                }
            }
        },
        clean: {
            target: ['target/'],
            dist: ['dist/']
        },
        copy: {
            providerStaticDev: {
                expand: true,
                cwd: 'static/',
                src: '**',
                dest: 'target/'
            },
            providerStaticDist: {
                expand: true,
                cwd: 'static/',
                src: '**',
                dest: 'dist/'
            }
        },
        watch: {
            static: {
                files: ['static/**/*'],
                tasks: ['copy:providerStaticDev']
            }
        },
        webpack: {
            options: {
                plugins: [new webpack.optimize.UglifyJsPlugin()],
                watch: true
            },
            buildConsumerDev: _.merge(_.cloneDeep(devOptions), consumerOptions),
            buildConsumerDist: _.merge(_.cloneDeep(distOptions), consumerOptions),
            buildProviderDev: _.merge(_.cloneDeep(devOptions), providerOptions),
            buildProviderDist: _.merge(_.cloneDeep(distOptions), providerOptions),
            buildViewerDev: _.merge(_.cloneDeep(devOptions), viewerOptions),
            buildViewerDist: _.merge(_.cloneDeep(distOptions), viewerOptions)
        }
    });

    grunt.registerTask('webpackDev', ["webpack:buildConsumerDev", "webpack:buildProviderDev", "webpack:buildViewerDev"]);
    grunt.registerTask('webpackDist', ["webpack:buildConsumerDist", "webpack:buildProviderDist", "webpack:buildViewerDist"]);
    grunt.registerTask("build", ["clean:target", "webpackDev", "copy:providerStaticDev"]);
    grunt.registerTask("serve", ["build", "connect", "watch"]);
    grunt.registerTask("dist", ["clean:dist", "webpackDist", "copy:providerStaticDist"]);
};
