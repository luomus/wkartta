'use strict';

var LajiMap = require('laji-map/dist/laji-map').default;
var $ = require('jquery');
var Uri = require('jsuri');
var JSON3 = require('json3');
var convertToGeoJSON = require('./provider/data-to-geojson');
var properties = require('json!../properties.json');

module.exports = function () {
    var DEFAULT_LAYER = 'openStreetMap';

    var render = function (params) {
      var tileLayerName = DEFAULT_LAYER;
      if (params.options.layerType === 'osm') {
        tileLayerName = 'openStreetMap';
      } else if (params.options.layerType === 'mm') {
        tileLayerName = 'maastokartta';
      }

      var rootElem = document.getElementById('map');
      var lajiMapOptions = {
        googleApiKey: properties.googleApiKey,
        rootElem: rootElem,
        tileLayerName: tileLayerName,
        lang: 'fi',
        draw: {
          geoData: convertToGeoJSON(params.data)
        },
        zoomToData: true
      };

      new LajiMap(lajiMapOptions);
    };

// Separated for testing purposes
    this._getUrl = function () {
        return new Uri(window.location.href);
    };

    this.main = function () {
        var config = decodeURIComponent(this._getUrl().anchor());
        if (config) {
            var params = JSON3.parse(config);
            render(params);
        }
    };
};
