'use strict';

var $ = require('jquery');
var convertToGeoJSON = require('./data-to-geojson');

module.exports = function (view, options) {
    var DEFAULT_GEOLOCATE_ZOOM = 15;
    var TYPE_MAPPINGS = {
        Point: 'POINT',
        LineString: 'LINESTRING',
        Polygon: 'POLYGON',
        Rectangle: 'POLYGON'
    };

    var entries = {};
    view.notes = function (id) {
        return entries[id].notes;
    };

    var getWKTCoordinates = function (leafletCoordinates) {
        var convert = function (value) {
          return [value.lat, value.lng];
        };
        // Single coordinates case
        if (leafletCoordinates.lat !== undefined && leafletCoordinates.lng !== undefined) {
            return convert(leafletCoordinates);
        } else {
            // List of coordinates
            // convert function is wrapped since jQuery flattens returned arrays...
            return $.map(leafletCoordinates, function(c) { return [convert(c)]; });
        }
    };

    var getLayerCoordinates = function (layer) {
      if (layer instanceof L.Marker) {
        return layer.getLatLng();
      } else if (layer instanceof L.Polygon) {
        return layer.getLatLngs()[0];
      } else if (layer instanceof L.Polyline) {
        return layer.getLatLngs();
      }
    };

    var thiz = this;
    $(view).on('created', function (e, layer) {
        if (TYPE_MAPPINGS[layer.feature.geometry.type] === undefined) {
            throw ('unknown layer type ' + layer.feature.geometry.type);
        }

        entries[layer._leaflet_id] = {
            locationType: TYPE_MAPPINGS[layer.feature.geometry.type],
            location: getWKTCoordinates(getLayerCoordinates(layer)),
            notes: ''
        };

        $(thiz).trigger('changed');
    });

    $(view).on('notesUpdated', function (e, layer, notes) {
        entries[layer._leaflet_id].notes = notes;
        $(thiz).trigger('changed');
    });

    $(view).on('edited', function (e, layer, id) {
        if (!entries[layer._leaflet_id]) {
          entries[layer._leaflet_id] = entries[id];
          delete entries[id];
        }
        entries[layer._leaflet_id].location = getWKTCoordinates(getLayerCoordinates(layer));
        $(thiz).trigger('changed');
    });

    $(view).on('deleted', function (e, layer) {
        delete entries[layer._leaflet_id];
        $(thiz).trigger('changed');
    });

    this.exportData = function () {
        return $.map(entries, function (value) {
            return value;
        });
    };

    this.importData = function (data) {
        function reverse(coordinates) {
            var reversed = []
            for (var i = 0; i < coordinates.length; i++) {
                reversed.push(coordinates[i].reverse());
            }
            return reversed;
        }
        view.clear();
        entries = {};
        var geometries = []
        var draw = view.lajimap.getDraw();
        draw.geoData = convertToGeoJSON(data);
        view.lajimap.updateDrawData(draw);
        view.reCenter();
    };

    this.geocode = function (query, zoomLevel) {
        if (typeof zoomLevel !== 'number') {
            zoomLevel = 15;
        }
        //bw compatibility with pre-LajiMap zoom levels.
        zoomLevel = zoomLevel - 3;
        view.lajimap.geocode(query, undefined, zoomLevel);
    };
};
