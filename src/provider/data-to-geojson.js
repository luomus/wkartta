'use strict';

var $ = require('jquery');

module.exports = function toGeoJSON(data) {
	function reverse(coordinates) {
		var reversed = []
		for (var i = 0; i < coordinates.length; i++) {
			reversed.push(coordinates[i].slice(0).reverse());
		}
		return reversed;
	}
	var geometries = []
	$.each(data, function () {
		switch (this.locationType) {
			case 'LINESTRING':
				geometries.push({type: 'LineString', coordinates: reverse(this.location)});
				break;
			case 'POINT':
				geometries.push({type: 'Point', coordinates: this.location.slice(0).reverse()});
				break;
			case 'POLYGON':
				geometries.push({type: 'Polygon', coordinates: [reverse(this.location)]});
				break;
			default:
				throw ('Unknown location type or not implemented yet: ' + data.locationType);
		}
	});
	return {type: 'GeometryCollection', geometries: geometries};
};
