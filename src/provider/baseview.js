'use strict';

var $ = require('jquery');
var properties = require('json!../../properties.json');
var DEFAULT_FIN_BOUNDS = [
    [59.445, 20.127],
    [70.021, 30.322]
];

module.exports = function (options) {
    var idsToTextAreas = {};
    var draw = {
        single: options.single || options.simple,
        circle: false
    };
    if (options.simple) {
        draw.rectangle = false;
        draw.polygon = false;
        draw.polyline = false;
    }
    var idxsToLayer = {};
    draw.onChange = function(events) {
        for (var i = 0; i < events.length; i++) {
            var event = events[i];
            switch(event.type) {
            case "create":
                var idx = lajimap.getDraw().featureCollection.features.length - 1;
                idxsToLayer[idx] = lajimap.getDrawLayerByIdx(idx);
                lajimap.map.fire("_add", {layer: lajimap.getDrawLayerByIdx(idx)});
                break;
            case "insert":
                idxsToLayer[e.idx] = lajimap.getDrawLayerByIdx(e.idx);
                lajimap.map.fire("_add", {layer: lajimap.getDrawLayerByIdx(e.idx)});
                break;
            case "edit":
                for (var idx in event.features) {
                    var oldId = idxsToLayer[idx]._leaflet_id;
                    idxsToLayer[idx] = lajimap.getDrawLayerByIdx(idx);
                    lajimap.map.fire("_edit", {layer: lajimap.getDrawLayerByIdx(idx), id: oldId});
                }
                break;
            case "delete":
                // Assume only one removal at a time
                if (!event.idxs.length) break;
                var idx = event.idxs[0];
                lajimap.map.fire("_delete", {layer: idxsToLayer[idx]});
                var length = Object.keys(idxsToLayer).length;
                for (var i = idx; i < length; i++) {
                    idxsToLayer[i] = idxsToLayer[i + 1];
                }
                delete idxsToLayer[Object.keys(idxsToLayer).length - 1];
            }
        }
    }

    if (options.annotate) {
        draw.getPopup = function(options, callback) {
						var idx = options.featureIdx;
						var geometry = options.geometry;
            var layer = idxsToLayer[idx];
            var id = L.Util.stamp(layer);

            var textarea = undefined;
            if (idsToTextAreas[id]) {
                textarea = idsToTextAreas[id];
            } else {
                textarea = $('<textarea rows="5" id="note"></textarea>');
                textarea.change(function (e) {
                    lajimap.map.fire("textareaChange", {layer: layer, value: textarea[0].value});
                });
            }

            idsToTextAreas[id] = textarea;

            setTimeout(function() {textarea[0].focus()}, 0);
            return textarea[0];
        }
    }

    var rootElem = document.getElementById('map');
    var lajiMapOptions = $.extend({
        googleApiKey: properties.googleApiKey,
        rootElem: rootElem,
        controls: {
            draw: {
                clear: true,
                delete: true
            }
        },
        draw: draw,
        lang: 'fi'
    }, (options.mapOptions || {}));

    lajiMapOptions.tileLayerName = 
        !options.mapTypes
        || options.mapTypes === 'auto'
        || Array.isArray(options.mapTypes) && (!options.mapTypes.length || options.mapTypes[0] === 'mm')
        ? 'maastokartta'
        : 'openStreetMap';

    var availableTileLayerNamesWhitelist = undefined;
    if (Array.isArray(options.mapTypes) && options.mapTypes.length === 1) {
        availableTileLayerNamesWhitelist = [];
        if (options.mapTypes.indexOf("osm") !== -1) {
            availableTileLayerNamesWhitelist.push("openStreetMap");
        }
        if (options.mapTypes.indexOf("mm") !== -1) {
            availableTileLayerNamesWhitelist.push("maastokartta");
        }
    }
    if (availableTileLayerNamesWhitelist) {
        lajiMapOptions.availableTileLayerNamesWhitelist = availableTileLayerNamesWhitelist;
    }

    if (options.mapOptions && options.mapOptions.scale === false) {
        lajiMapOptions.controls.scale = options.mapOptions.scale;
    }

    if (typeof lajiMapOptions.zoom !== 'number') {
        lajiMapOptions.zoom = 8;
    }

    var zoomKeys = ['zoom', 'maxZoom', 'minZoom'];
    for (var i = 0; i < zoomKeys.length; i++) {
        var key = zoomKeys[i];
        if (!lajiMapOptions.hasOwnProperty(key)) {
            return;
        }
        lajiMapOptions[key] = lajiMapOptions[key] - 3;
    }

    var lajimap = new LajiMap(lajiMapOptions);
    var map = lajimap.map;
    this._map = map;
    this.lajimap = lajimap;

    this.reCenter = function (latLng, zoomLevel) {
        if (latLng) {
            if (zoomLevel) {
                map.setView(latLng, lajimap.getDenormalizedZoom(zoomLevel));
            } else {
                map.setView(latLng);
            }
        } else {
            if (lajimap.getDraw().featureCollection.features.length) {
                lajimap.zoomToData();
            } else {
                lajimap.fitBounds(DEFAULT_FIN_BOUNDS);
            }
        }
    };

    this.notify = function (msg) {
        var $msgElem = $('#message');
        $msgElem.text(msg);
        $msgElem.fadeIn(500);
        setTimeout(function () {
            $msgElem.fadeOut(500);
        }, 1000 + 100 * msg.length);
    };

  this.clear = function () {
        lajimap.clearDrawData();
    };

};
