'use strict';

var BaseView = require('./baseview');
var $ = require('jquery');

module.exports = function (options) {
    var view = new BaseView(options);

    view._map.on('textareaChange', function (e) {
      var layer = e.layer;
      var value = e.value;
      $(view).trigger('notesUpdated', [layer, value]);
    });

    view._map.on('_add', function (e) {
        $(view).trigger('created', e.layer);
    });

    view._map.on('_delete', function (e) {
      $(view).trigger('deleted', e.layer);
    });

    view._map.on('_edit', function (e) {
      $(view).trigger('edited', [e.layer, e.id]);
    });

    return view;
};
