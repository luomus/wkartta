'use strict';

var Controller = require('./controller');
var View = require('./view');
var JSON3 = require('json3');
var ender = require('postmessage');
var jsUri = require('jsuri');
var $ = require('jquery');
var translations = require('json!./translations.json');

module.exports = function () {
    var DEFAULT_LOCALE = 'fi';

    // Separated for testing purposes
    this._getUrl = function () {
        return new jsUri(window.location.href);
    };

    this._initResources = function (locale) {
        var translation = translations[locale];
        if (translation === undefined) {
          translation = translations[DEFAULT_LOCALE];
        }

        // Extend the default leaflet ui strings
      //$.extend(L.drawLocal, translation);
    };

    this.main = function () {
        var config = this._getUrl().getQueryParamValue('config');

        if (config !== undefined) {
            var options = JSON3.parse(decodeURIComponent(config));
            var locale = options.locale || DEFAULT_LOCALE;
            this._initResources(locale);
            $('#loading').hide();
            this.init(options);
        }
    };

    this.init = function (options) {
        var isTouch = !!('ontouchstart' in window) || // works on most browsers
            !!(window.navigator.msMaxTouchPoints); // works on ie10

        // Fall back to simple view if using touchscreen as
        // leaflet.draw does not support touch
        var viewOptions = $.extend({}, options);
        viewOptions.mapOptions.center = new L.LatLng(options.mapOptions.center[0], options.mapOptions.center[1]);
        viewOptions.mapOptions.maxBounds = options.mapOptions.maxBounds ? new L.Bounds(options.mapOptions.maxBounds) : null;
        var view = new View(viewOptions);
        var controller = new Controller(view, options);

        var parentUrl = decodeURIComponent(window.location.hash.split('#')[1]);
        ender.receiveMessage(function (event) {
            var message;
            if (!event.data) {
              return;
            }
            try {
                message = JSON3.parse(event.data);
            } catch (e) {
                throw 'unable to parse message "' + event.data + '", error was:' + e;
            }
            if (message.poll) {
                ender.postMessage(JSON3.stringify({ready: true, target: options.target}), parentUrl);
            } else if (message.data) {
                controller.importData(message.data);
            } else if (message.geocode) {
                controller.geocode(message.geocode.query || null, message.geocode.zoomLevel || null);
            } else if (message.setView) {
                view.reCenter(message.setView.center, message.setView.zoomLevel);
            } else {
                throw "invalid message received: " + message;
            }
        });

        $(controller).on('changed', function () {
          try {
           ender.postMessage(JSON3.stringify({data: controller.exportData(), target: options.target}), parentUrl);
          } catch (e) {
            console.warn("No parent url found for map iframe");
          }
        });
    };
};
