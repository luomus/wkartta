'use strict';

var _ = require('lodash');
var JSON3 = require('json3');
var ender = require('postmessage');
var jsUri = require('jsuri');

module.exports = function () {
    this.DEFAULT_OPTIONS = {
        simple: true,
        single: true,
        annotate: true,
        mapOptions: {
            scale: true,
            center: [60, 20],
            maxBounds: null,
            maxZoom: 20,
            minZoom: null,
            zoom: 8
        },
        locationTypes: {
            lineString: true,
            point: true,
            polygon: true,
            rectangle: true
        },
        mapTypes: ['osm', 'mm']
    };
    var DEFAULT_OPTIONS = this.DEFAULT_OPTIONS;

    this.Map = function (mapElem, options) {
        options = options || {};
        options = _.extend(_.clone(DEFAULT_OPTIONS), options);
        var iframe = typeof(mapElem) === 'string' ? document.getElementById(mapElem) : mapElem;

        if (!iframe || !iframe.src) {
            throw (mapElem + ' is not a valid iframe');
        }

        ender.receiveMessage(function (event) {
            var message = JSON3.parse(event.data);
            if (message.ready) { // map has been loaded
                mapReady = true;

                // If there was data sent before the map was ready send it now
                if (cachedData) {
                    map.update(cachedData);
                }
            } else if (message.data) { // map was updated
                for (var i = 0; i < listeners.length; i++) {
                    listeners[i](message.data);
                }
            } else {
                throw 'invalid message ' + message;
            }
        });

        var newSrc = new jsUri(iframe.src);
        newSrc.setAnchor(encodeURIComponent(document.location.href));
        newSrc.addQueryParam('config', JSON3.stringify(options));
        iframe.src = newSrc.toString();

        var listeners = [];
        // If the consumer updates the map before it's loaded the
        var cachedData;
        var mapReady = false;

        var send = function (data) {
            ender.postMessage(JSON3.stringify(data), iframe.src, iframe.contentWindow);
        };

        var map = {options: options};
        map.update = function (mapEntries) {
            var payload = {data: mapEntries};
            if (mapReady) {
                send(payload);
            } else { // map is not ready yet, cache the data
                // only cache the latest message as we only care about the latest one
                cachedData = mapEntries;
            }
        };
        map.geocode = function (query, zoomLevel) {
            send({geocode: {query: query, zoomLevel: zoomLevel}});
        };
        map.setView = function (center, zoomLevel) {
            if (!center && !zoomLevel) {
                throw "either center or zoomLevel parameter must be defined";
            }
            send({setView: {center: center, zoomLevel: zoomLevel}});
        };
        map.onChange = function (callback) {
            listeners.push(callback);
        };

        var poller = function () {
            if (!mapReady) {
                send({poll: true});
                setTimeout(poller, 500);
            }
        };
        setTimeout(poller, 500);

        return map;
    };
};

